
	
	;import
		{RestrictingOmitFirst}
	 from										  'tshelpers/lib/types'
	;import
		{expectingTruthy
		,																  UnexpectedValueError}
	 from										  'tshelpers/lib/expectancy'
	
	;import{		countMatches}			from  'tshelpers/lib/functions'
	



	
/*	;import*as												 diff
	 from											   'deep-diff'
	;														 diff.DiffNew		.prototype.toString
	=														 diff.DiffDeleted	.prototype.toString
	=														 diff.DiffEdit		.prototype.toString
	=														 diff.DiffArray		.prototype.toString
	=											function						 toString		<		src,		tgt>
		(													 this
		 :													 diff.Diff							<		src,		tgt>)
		{switch(											 this.kind){case null:throw null //return'' // before rewriting .d.ts: aha: coding a case 'never' (à la `bash -O extglob -c 'case $v in(!(*));;esac'`) yields warning on incomparability with the (inferred) union, so I guess my underlying concern on braking-changes are settled: holy fuck the seemingly (looking across the web) only way to "discriminate" a union is by string - as by the scripture: https://www.typescriptlang.org/v2/docs/handbook/unions-and-intersections.html#discriminating-unions - I mean, I get the type is lost in the JSRuntimeEnvironment but is asking of TS to add conformance checks too much? // then again this reminisces of the discussion under https://www.typescriptlang.org/v2/docs/handbook/advanced-types.html#user-defined-type-guards - *sigh* - I miss Swift
			;break;					case	'D'	:const	d	=this as
															 diff.DiffDeleted					<		src				>
				;					return	'-	'+		d.													lhs
			;break;					case	'E'	:const	e	=this as
															 diff.DiffEdit						<		src,		tgt	>
				;					return	'-	'+		e.													lhs
										+ "\n+	"+		e.													rhs
			;break;					case	'N'	:const	n	=this as
															 diff.DiffNew						<					tgt	>
				;					return	'+	'+		n.													rhs
			;break;					case	'A'	:
				;					return	''	+			(this as
															 diff.DiffArray						<		src,		tgt	>)
					 .											  item
			;break;	default	:
							 throw new UnexpectedValueError (this.kind,"{kind} in instanceof require('deep-diff').Diff*")}
	;}/**/
	

	
	;				import{									 diff_match_patch
/*		,														  Diff
		as													 diff_Diff/**/}
	 from													'diff-match-patch'
	;		export	enum									 diff_Diff_Type
		{			negative=-1
		,			 normal = 0
		,			positive= 1}
	;		export	type									 diff_Diff
	=	[													 diff_Diff_Type,string]/**/
	;		declare	module									'diff-match-patch'
		{	export	interface								 diff_match_patch
			{ /** joins two arrays for:
				* @see https://github.com/google/diff-match-patch/wiki/Line-or-Word-Diffs wrapper:
				* @see this.diff_main()
				*/												  diff_lines
				<																L
				,																R>
				(										   array1			 :	L		[]
				,										   array2			 :	R		[]
				, /** default assigns "\n"	*/						 joinBy	?:	string
				, /** undefined -> false	*/							cleanupSemantic
				?:																boolean
				, /** @see this.diff_main() where undefined -> true
					*/							opt_checkLines				?:	boolean
				, /** @see this.diff_main() where undefined -> Number.MAX_VALUE
					*/							opt_deadline				?:	number			)
			:												 diff_Diff					[]	}}
	;														 diff_match_patch
	 .			prototype.										  diff_lines
	=			function										  diff_lines
		(												   array1
		,												   array2
				,													 joinBy	 = "\n"
				,														cleanupSemantic
				,								opt_checkLines
				,								opt_deadline)							{const
			{											   chars1
			,											   chars2,								lineArray}
		=													 diff.diff_linesToChars_
			(											   array1	.join
				(													 joinBy)
			,											   array2	.join
				(													 joinBy))			;const	r
		=													 diff.diff_main
			(											   chars1
			,											   chars2
			,									opt_checkLines
			,									opt_deadline)
		;													 diff.diff_charsToLines_	(		r,	lineArray)
		;if	(														   cleanupSemantic
			)												 diff.diff_cleanupSemantic	(		r)
		;return																					r}
	;		declare	module									'diff-match-patch'
		{	export	namespace								 diff_match_patch
			{export	namespace									  diff_lines_unify
				{type																							Formatter
				=	(	[				 typ
						,				 str]		 :		 diff_Diff
					,					 pfx								 :	string,							formatterLineCounter
					 :															number
					)=>															string		}	}
				interface									 diff_match_patch
			{													  diff_lines_unify // snake says: when in rome
				(												  diffs
				 :											 diff_Diff					[	]	,				delimiter
				?:																string			,				formatter
				?:											 diff_match_patch
				 .												  diff_lines_unify				.				Formatter
				):																string	[	]	}	}
	;														 diff_match_patch
	 .			prototype.										  diff_lines_unify
	=			function										  diff_lines_unify
		(														  diffs,										delimiter
		 = "\n"																					,				formatter)
		{let																									formatterLineCounter=0
		;								 return					  diffs
		 .map(													  diff		=>			{const[	typ
				,																				str
				]=												  diff
			;let						 pfx								 :	string
			;switch(																			typ)
				{		case		-1	:pfx	=  '-	' // unknown by HLJS - supported by Code
				;break;	case		 0	:pfx	=  '	' //			  common
				;break;	case		 1	:pfx	=  '+	' // known by HLJS - unsupported by Code
				;break;			default	:return	   '#	'	+new UnexpectedValueError(			typ,"Diff[0] in diff_lines_unify(diffs:Diff[]) where Diff is from require('diff-match-patch')")}
			;							 return													str		.split(	delimiter).slice(0,-1)//slice cause-of: this.diff_* seem to always add line-break at end
			 .map(																				par		=>		formatter
				?																								formatter
					(	[																		typ
						,																		par]
					,					 pfx,		++															formatterLineCounter)
				:						 pfx		+											par)	.join (	delimiter)})}
	
	;		export	const									 diff
	=new													 diff_match_patch
	;														 diff.Diff_Timeout=10 // default secondsFrom_diff_main_call 1.0; wh/ 0 is doc'd as "infinity" to be itty-bitty nit-picky nitty-gritty it's actually @./node_modules/diff-match-patch/index.js/diff_bisect_(): `for … if(Number.MAX_VALUE > (new Date).getTime()){then:break}` wh/ .getTime'd be millis
	

	
	;import															 {CustomError}
	 from														  'ts-custom-error'
	

	
	;declare	module										'vscode'
		{													 type	LanguageId=string}
	;import*as												 vscode
	 from													'vscode'
	

	
	;import{		   dummyLocalize}
	 from			'vscode-localizability'
	

	
	;		export							namespace		   UnmatchedLanguageError
		{	export								type					LanguageIdMatchable		= string/*
		;	export								type					LanguageId				= string/**/}
	//		export								type					Identifier				= string
	;		export								class		   UnmatchedLanguageError
	//		export								class		   UnmatchedIdentityError
	 extends															  CustomError{
	 	;		static		localize
		=			   dummyLocalize
		
		; /**
			* @see <static>`this.async` (pseudo) for an async factory
			*
			*/											constructor
			
			( /**	required cause-of async: `vscode==import('vscode')` and its `languages.getLanguages`
				*	@see `this.constructor` for @see <static>`this.async`
				*/						protected						languageIds
/*			 :												   UnmatchedLanguageError
			 .															LanguageId					[]/**/
			 :									 vscode.				LanguageId					[]/**/
			, /**	get w/o suffix defaults this instanceof Function by caching this,
				*		as this `.call(this, this)`
				*/						protected						languageIdMatchables_ // cspell:ignore matchables
			 :												   UnmatchedLanguageError
			 .															LanguageIdMatchable			[]
			| (	(e:											   UnmatchedLanguageError
				)=>											   UnmatchedLanguageError
				 .														LanguageIdMatchable			[]	)
			, /**	get w/o suffix defaults this instanceof Function by caching this,
				*		as this `.call(this, this)`
				*/						protected						languageIdMatchableSuggestion_
			 :		undefined|								   UnmatchedLanguageError
			 .															LanguageIdMatchable
			| (	(e:											   UnmatchedLanguageError
				)=>	undefined|								   UnmatchedLanguageError
				 .														LanguageIdMatchable				)
			, /**	constructor uses in defaulting `this.messageWithDirectionsToComparison` by doing `this.computeDefaultMessage` of this
				*/																				 directionsToComparison
			?:																	  string
			, /**	with vscode==import('vscode'), and `diff==augmented(import('diff-match-patch'))`,
				*		defaulting `this.comparisonString` will when truthy do `this.comparisonLinks.push` of `vscode.DocumentLink` with `{target:`return of this`}`
				*/						public									comparisonStringDefaultAddsLinks
			?:	(...args:Parameters<							  diff_match_patch
					 .											  diff_lines_unify				.				Formatter>)=>undefined
					|							 vscode.Uri
					|	[						 vscode.Uri
						,undefined|												  string]
			, /**/						public									comparisonStringDefaultAddsActions
			?:	(...args:Parameters<							  diff_match_patch
					 .											  diff_lines_unify				.				Formatter>)=>undefined
					|							 vscode.CodeAction
			, /**	base for get defaults: `languageId`, and `languageIdSuggestion`;
				*	get w/o suffix defaults by caching this,
				*		as `vscode.window.activeTextEditor`
				*	@throws `UnexpectedValueError` on remaining fault
				*/						protected						document_
			?:									 vscode.			TextDocument
			, /**	get w/o suffix defaults by caching this,
				*		as it of `document`
				*/						protected						languageId_
/*			?:												   UnmatchedLanguageError
			 .															LanguageId/**/
			?:									 vscode.				LanguageId/**/
			, /**	get w/o suffix defaults by caching this with `hljs==import('highlight.js')`, and `diff==augmented(import('diff-match-patch'))`,
				*		as `diff.diff_lines` of between `hljs.listLanguages` and `this.languageIds`
				*	@note defaulting `this.comparisonString` will have the former and latter of this prefixed with "+" and "-", respectively and correspondingly with their localized header lines, or nothing when equal, and always fused by a tab
				*/						protected								comparison_
			?:												 diff_Diff								[]
			, /**	get w/o suffix defaults by caching this with diff==augmented(import('diff-match-patch')),
				*		as `this.comparison.map(diff.diff_line_unify).join("\n")`
				*	@see `this.comparison_` and its at-note
				*/						protected								comparisonString_
			?:																	  string/**/
			, /**	constructor defaults by `this.computeDefaultMessage(directionsToComparison)`
				*/						public										  messageWithDirectionsToComparison
			?:																	  string
			,																		  message
			?:																	  string)
			{												super()
			;												this.					  message
			=																		  message
			||												this.		computeDefaultMessage()
			;												this.					  messageWithDirectionsToComparison
			=																		  messageWithDirectionsToComparison
			||												this.		computeDefaultMessage(	 directionsToComparison)
			}
		;																computeDefaultMessage
			( /** if truthy replaces `comparisonString`
				*/																				 directionsToComparison
			?:																	  string)
			{const	  n="\n"
			;return	localize(								  'UnmatchedLanguageError.message'
//				+	''	+(!								(										 directionsToComparison	)?'':'.withDirectionsToComparison')
				+	''	+(!								(	this.				comparison
					instanceof													Error									)?'':'.whereComparisonIsError')
				,											  "Unmatched language, determining syntax for comments"
				+ ": "	+										"Code currently doesn't give extensions access to its LanguageConfigurationRegistry"
				+  ' '	+											"(feature-request: https://github.com/Microsoft/vscode/issues/20735)"
				+ "." +n+										"highlight.js is a general good stand-in to this purpose"
				+ ", but "+										"highlight.js has no exact match for"
				+  ' '	+											'"{0}"'
				+ ". "	+										"highlight.js:s guess is"
				+  ' '	+											'"{1}"'
				+ "." +n+( 									this.				comparison
				  instanceof													Error
				  ?' '	+										"An error occurred trying to compare Code and highlight.js:s languages"
				  :' '	+										"A comparison between Code and highlight.js:s languages should be available")
				+ ":" +n+											 '{2}'
				+	   n+										"Would support be possible, a mapping need to be added to this extension's source manually"
				+  ' '	+											"as it is not configurable (yet)."
				,											this.		languageId
				,											this.		languageIdMatchableSuggestion					||	 undefined
				,																				 directionsToComparison
				||											this.				comparisonString
				)
			}
		;								public				get			languageIdMatchables
		  (){return										!(	this.		languageIdMatchables_
				instanceof Function						)?	this.		languageIdMatchables_
			:	(											this.		languageIdMatchables_
				=											this.		languageIdMatchables_
					(										this))}
		;								public				get			languageIdMatchableSuggestion
		  (){return										!(	this.		languageIdMatchableSuggestion_
			instanceof Function							)?	this.		languageIdMatchableSuggestion_
			:	(											this.		languageIdMatchableSuggestion_
				=											this.		languageIdMatchableSuggestion_
					(										this))}
		;								public				get			document
		  (){return											this.		document_
			|| (											this.		document_
				=	expectingTruthy(			 vscode . window
				 .											  activeTextEditor
				,											 "activeTextEditor")
				 .														document)}
		;								public				get			languageId
		  (){return											this.		languageId_
			|| (											this.		languageId_
				=											this.		document
				 .														languageId)}
		;								public				get					comparison()
			{return											this.				comparison_
			||	(											this.				comparison_
				=											diff.diff_lines
					(										this.		languageIds	.sort()
					,										this.		languageIdMatchables
					,			"\n"))}
		;								public									comparisonLinks
		 :Array<								 vscode .						  DocumentLink				>=[]
		;								public									comparisonRangeAndActionCouple
		 :Array<readonly[						 vscode .								  Range
				,								 vscode .									  CodeAction]	>=[]
		;								public				get					comparisonString()
			{const					header =			   '--- '	+localize('UnmatchedLanguageError.unknown'	,'{0}',vscode.env.appName	,	/**/	/*	,' not  1974-01-01 00:00:00.000000000 +0000'/**/)
			+									"\n"+	   '+++ '	+localize('UnmatchedLanguageError.known'	,'{0}','highlight.js'		,	/**/	/*	,'known 1974-01-01 00:00:00.000000000 +0000'/**/)
//			+									"\n"+	   '@@ -1 +1 @@'
			,						headerLineCount
			=								  countMatches
				(								/\n/g
				,					header)
			;return											this.				comparisonString_
			||	(											this.				comparisonString_
				=					header	+	"\n"+		diff.diff_lines_unify
				  (											this.				comparison
				  ,								"\n",	!	this.				comparisonStringDefaultAddsLinks
				  &&									!	this.				comparisonStringDefaultAddsActions		?undefined
				  :	(	[				typ
						,				str],	pfx
					,					  line)=>
					{const					ret=pfx
					+					str
					,			 range	=	new	 vscode.Range
						(					new	 vscode.Position
							(			  line
							+=		headerLineCount
							,1		+	typ) // "\t".length === 1
						,					new	 vscode.Position
							(			  line
							,				ret.length))
					;if(									this.				comparisonStringDefaultAddsLinks)
					  {const																		   addsLinkTarget_possiblyWithTooltip
					  =										this.				comparisonStringDefaultAddsLinks
						(	[			typ
							,			str],	pfx
						,				  line)
					  ;if(																			   addsLinkTarget_possiblyWithTooltip){
						;if	(																		   addsLinkTarget_possiblyWithTooltip instanceof Array
							)var[																	   addsLinkTarget
								,																	   addsLinkTargetTooltip]
							=																		   addsLinkTarget_possiblyWithTooltip
						else var																	   addsLinkTarget
							=																		   addsLinkTarget_possiblyWithTooltip
						;									this.				comparisonLinks
						 .push(	{range
								:range
								,target	:															   addsLinkTarget
								,tooltip:															   addsLinkTargetTooltip}) // tooltip: prefixed to eg on macOS "(cmd + click)", prefixing referred to as `{0} (ctrl + click)` in the doc-block
						;}}
					;if(									this.				comparisonStringDefaultAddsActions)
					  {const																		   addsAction
					  =										this.				comparisonStringDefaultAddsActions
						(	[			typ
							,			str],	pfx
						,				  line)
					  ;if(																			   addsAction){
						;									this.				comparisonRangeAndActionCouple
						 .push(	[range	,															   addsAction])
						;}}
					;return					ret})
				 .join(							"\n"))}
		; /** {@inheritdoc UnmatchedLanguageError."constructor"}
			* @see constructor params (and props)
			*/									static	async			async(				...args
			:RestrictingOmitFirst<ConstructorParameters<typeof UnmatchedLanguageError>
/*				,											   UnmatchedLanguageError
				 .														LanguageId[]/**/
				,								 vscode.				LanguageId[]/**/>)
			{return								 vscode .				languages
			 .														 getLanguages().then($=>
														   new UnmatchedLanguageError	($,	...args))}
		;}
	

	
	;import{				localizeBy}
	 from			'vscode-localizability'
	;const					localize
	=						localizeBy(						   UnmatchedLanguageError)
	

