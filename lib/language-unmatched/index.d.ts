import { RestrictingOmitFirst } from 'tshelpers/lib/types';
import { diff_match_patch } from 'diff-match-patch';
export declare enum diff_Diff_Type {
    negative = -1,
    normal = 0,
    positive = 1
}
export declare type diff_Diff = [diff_Diff_Type, string];
declare module 'diff-match-patch' {
    interface diff_match_patch {
        diff_lines<L, R>(array1: L[], array2: R[], /** default assigns "\n"	*/ joinBy?: string, /** undefined -> false	*/ cleanupSemantic?: boolean, /** @see this.diff_main() where undefined -> true
            */ opt_checkLines?: boolean, /** @see this.diff_main() where undefined -> Number.MAX_VALUE
            */ opt_deadline?: number): diff_Diff[];
    }
}
declare module 'diff-match-patch' {
    namespace diff_match_patch {
        namespace diff_lines_unify {
            type Formatter = ([typ, str]: diff_Diff, pfx: string, formatterLineCounter: number) => string;
        }
    }
    interface diff_match_patch {
        diff_lines_unify(diffs: diff_Diff[], delimiter?: string, formatter?: diff_match_patch.diff_lines_unify.Formatter): string[];
    }
}
export declare const diff: diff_match_patch;
import { CustomError } from 'ts-custom-error';
declare module 'vscode' {
    type LanguageId = string;
}
import * as vscode from 'vscode';
import { dummyLocalize } from 'vscode-localizability';
export declare namespace UnmatchedLanguageError {
    type LanguageIdMatchable = string;
}
export declare class UnmatchedLanguageError extends CustomError {
    protected languageIds: vscode.LanguageId[];
    protected languageIdMatchables_: UnmatchedLanguageError.LanguageIdMatchable[] | ((e: UnmatchedLanguageError) => UnmatchedLanguageError.LanguageIdMatchable[]);
    protected languageIdMatchableSuggestion_: undefined | UnmatchedLanguageError.LanguageIdMatchable | ((e: UnmatchedLanguageError) => undefined | UnmatchedLanguageError.LanguageIdMatchable);
    comparisonStringDefaultAddsLinks?: ((__0: diff_Diff, pfx: string, formatterLineCounter: number) => vscode.Uri | [vscode.Uri, string | undefined] | undefined) | undefined;
    comparisonStringDefaultAddsActions?: ((__0: diff_Diff, pfx: string, formatterLineCounter: number) => vscode.CodeAction | undefined) | undefined;
    protected document_?: vscode.TextDocument | undefined;
    protected languageId_?: string | undefined;
    protected comparison_?: diff_Diff[] | undefined;
    protected comparisonString_?: string | undefined;
    messageWithDirectionsToComparison?: string | undefined;
    static localize: typeof dummyLocalize; /**
        * @see <static>`this.async` (pseudo) for an async factory
        *
        */
    constructor(/**	required cause-of async: `vscode==import('vscode')` and its `languages.getLanguages`
        *	@see `this.constructor` for @see <static>`this.async`
        */ languageIds: vscode.LanguageId[], /**	get w/o suffix defaults this instanceof Function by caching this,
        *		as this `.call(this, this)`
        */ languageIdMatchables_: UnmatchedLanguageError.LanguageIdMatchable[] | ((e: UnmatchedLanguageError) => UnmatchedLanguageError.LanguageIdMatchable[]), /**	get w/o suffix defaults this instanceof Function by caching this,
        *		as this `.call(this, this)`
        */ languageIdMatchableSuggestion_: undefined | UnmatchedLanguageError.LanguageIdMatchable | ((e: UnmatchedLanguageError) => undefined | UnmatchedLanguageError.LanguageIdMatchable), /**	constructor uses in defaulting `this.messageWithDirectionsToComparison` by doing `this.computeDefaultMessage` of this
        */ directionsToComparison?: string, /**	with vscode==import('vscode'), and `diff==augmented(import('diff-match-patch'))`,
        *		defaulting `this.comparisonString` will when truthy do `this.comparisonLinks.push` of `vscode.DocumentLink` with `{target:`return of this`}`
        */ comparisonStringDefaultAddsLinks?: ((__0: diff_Diff, pfx: string, formatterLineCounter: number) => vscode.Uri | [vscode.Uri, string | undefined] | undefined) | undefined, /**/ comparisonStringDefaultAddsActions?: ((__0: diff_Diff, pfx: string, formatterLineCounter: number) => vscode.CodeAction | undefined) | undefined, /**	base for get defaults: `languageId`, and `languageIdSuggestion`;
        *	get w/o suffix defaults by caching this,
        *		as `vscode.window.activeTextEditor`
        *	@throws `UnexpectedValueError` on remaining fault
        */ document_?: vscode.TextDocument | undefined, /**	get w/o suffix defaults by caching this,
        *		as it of `document`
        */ languageId_?: string | undefined, /**	get w/o suffix defaults by caching this with `hljs==import('highlight.js')`, and `diff==augmented(import('diff-match-patch'))`,
        *		as `diff.diff_lines` of between `hljs.listLanguages` and `this.languageIds`
        *	@note defaulting `this.comparisonString` will have the former and latter of this prefixed with "+" and "-", respectively and correspondingly with their localized header lines, or nothing when equal, and always fused by a tab
        */ comparison_?: diff_Diff[] | undefined, /**	get w/o suffix defaults by caching this with diff==augmented(import('diff-match-patch')),
        *		as `this.comparison.map(diff.diff_line_unify).join("\n")`
        *	@see `this.comparison_` and its at-note
        */ comparisonString_?: string | undefined, /**	constructor defaults by `this.computeDefaultMessage(directionsToComparison)`
        */ messageWithDirectionsToComparison?: string | undefined, message?: string);
    computeDefaultMessage(/** if truthy replaces `comparisonString`
        */ directionsToComparison?: string): string;
    get languageIdMatchables(): string[];
    get languageIdMatchableSuggestion(): string | undefined;
    get document(): vscode.TextDocument;
    get languageId(): string;
    get comparison(): diff_Diff[];
    comparisonLinks: Array<vscode.DocumentLink>;
    comparisonRangeAndActionCouple: Array<readonly [vscode.Range, vscode.CodeAction]>;
    get comparisonString(): string;
    static async(...args: RestrictingOmitFirst<ConstructorParameters<typeof UnmatchedLanguageError>, vscode.LanguageId[]>): Promise<UnmatchedLanguageError>;
}
//# sourceMappingURL=index.d.ts.map