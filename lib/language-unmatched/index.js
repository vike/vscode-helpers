"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
;
const expectancy_1 = require("tshelpers/lib/expectancy");
const functions_1 = require("tshelpers/lib/functions");
const diff_match_patch_1 = require("diff-match-patch");
var diff_Diff_Type;
(function (diff_Diff_Type) {
    diff_Diff_Type[diff_Diff_Type["negative"] = -1] = "negative";
    diff_Diff_Type[diff_Diff_Type["normal"] = 0] = "normal";
    diff_Diff_Type[diff_Diff_Type["positive"] = 1] = "positive";
})(diff_Diff_Type = exports.diff_Diff_Type || (exports.diff_Diff_Type = {}));
;
;
diff_match_patch_1.diff_match_patch
    .prototype.diff_lines
    = function diff_lines(array1, array2, joinBy = "\n", cleanupSemantic, opt_checkLines, opt_deadline) {
        const { chars1, chars2, lineArray } = exports.diff.diff_linesToChars_(array1.join(joinBy), array2.join(joinBy));
        const r = exports.diff.diff_main(chars1, chars2, opt_checkLines, opt_deadline);
        exports.diff.diff_charsToLines_(r, lineArray);
        if (cleanupSemantic)
            exports.diff.diff_cleanupSemantic(r);
        return r;
    };
;
diff_match_patch_1.diff_match_patch
    .prototype.diff_lines_unify
    = function diff_lines_unify(diffs, delimiter = "\n", formatter) {
        let formatterLineCounter = 0;
        return diffs
            .map(diff => {
            const [typ, str] = diff;
            let pfx;
            switch (typ) {
                case -1:
                    pfx = '-	' // unknown by HLJS - supported by Code
                    ;
                    break;
                case 0:
                    pfx = '	' //			  common
                    ;
                    break;
                case 1:
                    pfx = '+	' // known by HLJS - unsupported by Code
                    ;
                    break;
                default: return '#	' + new expectancy_1.UnexpectedValueError(typ, "Diff[0] in diff_lines_unify(diffs:Diff[]) where Diff is from require('diff-match-patch')");
            }
            ;
            return str.split(delimiter).slice(0, -1) //slice cause-of: this.diff_* seem to always add line-break at end
                .map(par => formatter
                ? formatter([typ,
                    par], pfx, ++formatterLineCounter)
                : pfx + par).join(delimiter);
        });
    };
exports.diff = new diff_match_patch_1.diff_match_patch;
exports.diff.Diff_Timeout = 10 // default secondsFrom_diff_main_call 1.0; wh/ 0 is doc'd as "infinity" to be itty-bitty nit-picky nitty-gritty it's actually @./node_modules/diff-match-patch/index.js/diff_bisect_(): `for … if(Number.MAX_VALUE > (new Date).getTime()){then:break}` wh/ .getTime'd be millis
;
const ts_custom_error_1 = require("ts-custom-error");
;
const vscode = require("vscode");
const vscode_localizability_1 = require("vscode-localizability");
//		export								type					Identifier				= string
;
class UnmatchedLanguageError
//		export								class		   UnmatchedIdentityError
 extends ts_custom_error_1.CustomError {
    constructor(/**	required cause-of async: `vscode==import('vscode')` and its `languages.getLanguages`
        *	@see `this.constructor` for @see <static>`this.async`
        */ languageIds /**/, /**	get w/o suffix defaults this instanceof Function by caching this,
        *		as this `.call(this, this)`
        */ languageIdMatchables_ // cspell:ignore matchables
    , /**	get w/o suffix defaults this instanceof Function by caching this,
        *		as this `.call(this, this)`
        */ languageIdMatchableSuggestion_, /**	constructor uses in defaulting `this.messageWithDirectionsToComparison` by doing `this.computeDefaultMessage` of this
        */ directionsToComparison, /**	with vscode==import('vscode'), and `diff==augmented(import('diff-match-patch'))`,
        *		defaulting `this.comparisonString` will when truthy do `this.comparisonLinks.push` of `vscode.DocumentLink` with `{target:`return of this`}`
        */ comparisonStringDefaultAddsLinks, /**/ comparisonStringDefaultAddsActions, /**	base for get defaults: `languageId`, and `languageIdSuggestion`;
        *	get w/o suffix defaults by caching this,
        *		as `vscode.window.activeTextEditor`
        *	@throws `UnexpectedValueError` on remaining fault
        */ document_, /**	get w/o suffix defaults by caching this,
        *		as it of `document`
        */ languageId_ /**/, /**	get w/o suffix defaults by caching this with `hljs==import('highlight.js')`, and `diff==augmented(import('diff-match-patch'))`,
        *		as `diff.diff_lines` of between `hljs.listLanguages` and `this.languageIds`
        *	@note defaulting `this.comparisonString` will have the former and latter of this prefixed with "+" and "-", respectively and correspondingly with their localized header lines, or nothing when equal, and always fused by a tab
        */ comparison_, /**	get w/o suffix defaults by caching this with diff==augmented(import('diff-match-patch')),
        *		as `this.comparison.map(diff.diff_line_unify).join("\n")`
        *	@see `this.comparison_` and its at-note
        */ comparisonString_ /**/, /**	constructor defaults by `this.computeDefaultMessage(directionsToComparison)`
        */ messageWithDirectionsToComparison, message) {
        super();
        this.languageIds = languageIds;
        this.languageIdMatchables_ // cspell:ignore matchables
         = languageIdMatchables_;
        this.languageIdMatchableSuggestion_ = languageIdMatchableSuggestion_;
        this.comparisonStringDefaultAddsLinks = comparisonStringDefaultAddsLinks;
        this.comparisonStringDefaultAddsActions = comparisonStringDefaultAddsActions;
        this.document_ = document_;
        this.languageId_ = languageId_;
        this.comparison_ = comparison_;
        this.comparisonString_ = comparisonString_;
        this.messageWithDirectionsToComparison = messageWithDirectionsToComparison;
        this.comparisonLinks = [];
        this.comparisonRangeAndActionCouple = [];
        this.message
            = message
                || this.computeDefaultMessage();
        this.messageWithDirectionsToComparison
            = messageWithDirectionsToComparison
                || this.computeDefaultMessage(directionsToComparison);
    }
    ;
    ;
    computeDefaultMessage(/** if truthy replaces `comparisonString`
        */ directionsToComparison) {
        const n = "\n";
        return localize('UnmatchedLanguageError.message'
            //				+	''	+(!								(										 directionsToComparison	)?'':'.withDirectionsToComparison')
            + '' + (!(this.comparison
            instanceof Error) ? '' : '.whereComparisonIsError'), "Unmatched language, determining syntax for comments"
            + ": " + "Code currently doesn't give extensions access to its LanguageConfigurationRegistry"
            + ' ' + "(feature-request: https://github.com/Microsoft/vscode/issues/20735)"
            + "." + n + "highlight.js is a general good stand-in to this purpose"
            + ", but " + "highlight.js has no exact match for"
            + ' ' + '"{0}"'
            + ". " + "highlight.js:s guess is"
            + ' ' + '"{1}"'
            + "." + n + (this.comparison
            instanceof Error
            ? ' ' + "An error occurred trying to compare Code and highlight.js:s languages"
            : ' ' + "A comparison between Code and highlight.js:s languages should be available")
            + ":" + n + '{2}'
            + n + "Would support be possible, a mapping need to be added to this extension's source manually"
            + ' ' + "as it is not configurable (yet).", this.languageId, this.languageIdMatchableSuggestion || undefined, directionsToComparison
            || this.comparisonString);
    }
    ;
    get languageIdMatchables() {
        return !(this.languageIdMatchables_
            instanceof Function) ? this.languageIdMatchables_
            : (this.languageIdMatchables_
                = this.languageIdMatchables_(this));
    }
    ;
    get languageIdMatchableSuggestion() {
        return !(this.languageIdMatchableSuggestion_
            instanceof Function) ? this.languageIdMatchableSuggestion_
            : (this.languageIdMatchableSuggestion_
                = this.languageIdMatchableSuggestion_(this));
    }
    ;
    get document() {
        return this.document_
            || (this.document_
                = expectancy_1.expectingTruthy(vscode.window
                    .activeTextEditor, "activeTextEditor")
                    .document);
    }
    ;
    get languageId() {
        return this.languageId_
            || (this.languageId_
                = this.document
                    .languageId);
    }
    ;
    get comparison() {
        return this.comparison_
            || (this.comparison_
                = exports.diff.diff_lines(this.languageIds.sort(), this.languageIdMatchables, "\n"));
    }
    ;
    get comparisonString() {
        const header = '--- ' + localize('UnmatchedLanguageError.unknown', '{0}', vscode.env.appName)
            + "\n" + '+++ ' + localize('UnmatchedLanguageError.known', '{0}', 'highlight.js')
        //			+									"\n"+	   '@@ -1 +1 @@'
        , headerLineCount = functions_1.countMatches(/\n/g, header);
        return this.comparisonString_
            || (this.comparisonString_
                = header + "\n" + exports.diff.diff_lines_unify(this.comparison, "\n", !this.comparisonStringDefaultAddsLinks
                    && !this.comparisonStringDefaultAddsActions ? undefined
                    : ([typ, str], pfx, line) => {
                        const ret = pfx
                            + str, range = new vscode.Range(new vscode.Position(line
                            += headerLineCount, 1 + typ) // "\t".length === 1
                        , new vscode.Position(line, ret.length));
                        if (this.comparisonStringDefaultAddsLinks) {
                            const addsLinkTarget_possiblyWithTooltip = this.comparisonStringDefaultAddsLinks([typ,
                                str], pfx, line);
                            if (addsLinkTarget_possiblyWithTooltip) {
                                ;
                                if (addsLinkTarget_possiblyWithTooltip instanceof Array)
                                    var [addsLinkTarget, addsLinkTargetTooltip] = addsLinkTarget_possiblyWithTooltip;
                                else
                                    var addsLinkTarget = addsLinkTarget_possiblyWithTooltip;
                                this.comparisonLinks
                                    .push({ range: range,
                                    target: addsLinkTarget,
                                    tooltip: addsLinkTargetTooltip }) // tooltip: prefixed to eg on macOS "(cmd + click)", prefixing referred to as `{0} (ctrl + click)` in the doc-block
                                ;
                            }
                        }
                        ;
                        if (this.comparisonStringDefaultAddsActions) {
                            const addsAction = this.comparisonStringDefaultAddsActions([typ,
                                str], pfx, line);
                            if (addsAction) {
                                ;
                                this.comparisonRangeAndActionCouple
                                    .push([range, addsAction]);
                            }
                        }
                        ;
                        return ret;
                    })
                    .join("\n"));
    }
    ; /** {@inheritdoc UnmatchedLanguageError."constructor"}
        * @see constructor params (and props)
        */
    static async(...args) {
        return __awaiter(this, void 0, void 0, function* () {
            return vscode.languages
                .getLanguages().then($ => new UnmatchedLanguageError($, ...args));
        });
    }
    ;
}
exports.UnmatchedLanguageError = UnmatchedLanguageError;
UnmatchedLanguageError.localize = vscode_localizability_1.dummyLocalize; /**
    * @see <static>`this.async` (pseudo) for an async factory
    *
    */
;
const vscode_localizability_2 = require("vscode-localizability");
const localize = vscode_localizability_2.localizeBy(UnmatchedLanguageError);
//# sourceMappingURL=index.js.map